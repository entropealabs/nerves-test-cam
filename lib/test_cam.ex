defmodule TestCam do
  @moduledoc """
  Documentation for TestCam.
  """

  @doc """
  Hello world.

  ## Examples

      iex> TestCam.hello
      :world

  """
  def hello do
    :world
  end
end
